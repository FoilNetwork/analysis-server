package main

import (
	"FoilNetwork/analysis-server/pkg/app/config"
	"FoilNetwork/analysis-server/pkg/app/server"
	"FoilNetwork/analysis-server/pkg/app/store"
	"FoilNetwork/analysis-server/pkg/app/worker"
	"log"
)

// @title FoilNetwork Analysis Server API
// @version 0.1.0
// @description The service is used for storing, processing and issuing data on transactions.

// @host 165.227.132.227:8080
// @BasePath /

func main() {
	cfg, err := config.Init()
	if err != nil {
		log.Fatal(err)
	}

	if cfg.DebugMode {
		log.SetFlags(log.LstdFlags | log.Lshortfile)
	}

	//db
	ds := "host=192.168.56.104 user=app dbname=db password=pass sslmode=disable "

	db, err := store.NewDB(ds)
	if err != nil {
		log.Fatal("unable to connect to the database:", err)
	}

	defer db.Close()

	//worker
	w := worker.New(db, cfg.Interval, cfg.Nodes)
	w.Setup()

	//router
	app := server.New(cfg.DebugMode, db)

	if err := app.Start(cfg.Addr); err != nil {
		log.Fatal(err)
	}
}

//TODO: set error trace := fmt.Sprintf("%s\n%s", err.Error(), debug.Stack())
