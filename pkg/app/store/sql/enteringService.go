package sql

import (
	"FoilNetwork/analysis-server/pkg/app/store/model"
	"context"
	"errors"
	"fmt"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"log"
	"time"
)

type Trades struct {
	pool *pgxpool.Pool
}

func NewTradesService(pool *pgxpool.Pool) *Trades {
	return &Trades{pool: pool}
}

func (srv *Trades) GetLast(ctx context.Context, block *model.Block) (*model.Trade, error) {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	trade := &model.Trade{}
	statement := fmt.Sprintf("SELECT %s FROM trades WHERE genesis_signature = $1 ORDER BY height DESC LIMIT 1",
		model.TradeColumns())

	err := srv.pool.QueryRow(ctx, statement, block.Signature).
		Scan(trade.Fields()...)
	if err != nil && !errors.Is(err, pgx.ErrNoRows) {
		return trade, err
	}

	return trade, nil
}

func (srv *Trades) SaveData(ctx context.Context, block *model.Block, trades []*model.Trade) error {
	ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()

	var id string

	tx, err := srv.pool.Begin(ctx)
	if err != nil {
		return err
	}

	for _, trade := range trades {
		statement := fmt.Sprintf(
			`INSERT INTO trades (%s) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15) ON CONFLICT DO NOTHING RETURNING trade_id;`,
			model.TradeColumns())

		//set genesis block signature
		trade.GenesisBlock = block.Signature

		err := tx.QueryRow(ctx, statement, trade.Fields()...).Scan(&id)
		if err != nil && !errors.Is(err, pgx.ErrNoRows) {
			log.Println(err)
			if err := tx.Rollback(ctx); err != nil {
				return err
			}
			return err
		}
	}

	return tx.Commit(ctx)
}

func (srv *Trades) Get(ctx context.Context, baseId int, quoteId int, timestamp int64, signature string) ([]*model.Trade, error) {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	statement := fmt.Sprintf(`SELECT %s FROM trades t WHERE t.want_key = $1 AND t.have_key = $2 AND t.timestamp >= $3
									AND genesis_signature = $4 ORDER BY timestamp DESC;`,
		model.TradeColumns())

	rows, err := srv.pool.Query(ctx, statement, baseId, quoteId, timestamp, signature)
	if err != nil && !errors.Is(err, pgx.ErrNoRows) {
		return nil, err
	}

	defer rows.Close()
	var trades []*model.Trade

	for rows.Next() {
		t := &model.Trade{}
		err := rows.Scan(t.Fields()...)
		if err != nil {
			return nil, err
		}
		trades = append(trades, t)
	}

	return trades, err
}

func (srv *Trades) GetByTime(ctx context.Context, timestamp int64, signature string) ([]*model.Trade, error) {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	statement := fmt.Sprintf(`SELECT %s FROM trades WHERE timestamp > $1 AND genesis_signature = $2`, model.TradeColumns())
	rows, err := srv.pool.Query(ctx, statement, timestamp, signature)
	if err != nil && !errors.Is(err, pgx.ErrNoRows) {
		return nil, err
	}

	defer rows.Close()
	var trades []*model.Trade

	for rows.Next() {
		t := &model.Trade{}
		err := rows.Scan(t.Fields()...)
		if err != nil {
			return nil, err
		}
		trades = append(trades, t)
	}

	return trades, err
}
