package sql

import (
	"FoilNetwork/analysis-server/pkg/app/store/model"
	"context"
	"errors"
	"fmt"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"log"
	"time"
)

type Chart struct {
	pool *pgxpool.Pool
}

func NewChartRepository(pool *pgxpool.Pool) *Chart {
	return &Chart{pool: pool}
}

func (srv *Chart) GetLast(ctx context.Context, signature string) (map[string]model.OHLC, error) {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	statement := `SELECT pair_id, MAX(end_time) FROM ohlc
					WHERE genesis_signature = $1
					GROUP BY pair_id`

	rows, err := srv.pool.Query(ctx, statement, signature)
	if err != nil && !errors.Is(err, pgx.ErrNoRows) {
		log.Println(err)
		return nil, err
	}

	defer rows.Close()

	chart := make(map[string]model.OHLC)

	for rows.Next() {
		c := model.OHLC{}

		err := rows.Scan(&c.PairID, &c.EndTime)
		if err != nil {
			log.Println(err)
			return nil, err
		}

		chart[c.PairID] = c
	}

	return chart, nil
}

func (srv *Chart) Insert(ctx context.Context, block *model.Block, chartsByPair map[string]map[int64]*model.OHLC) error {
	ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()

	var endTime int64

	tx, err := srv.pool.Begin(ctx)
	if err != nil {
		return err
	}

	for _, charts := range chartsByPair {

		for _, chart := range charts {
			statement := fmt.Sprintf(
				`INSERT INTO ohlc (%s) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12) RETURNING end_time;`, // ON CONFLICT DO NOTHING
				model.OHLCColumns())

			chart.GenesisBlock = block.Signature

			err := tx.QueryRow(ctx, statement, chart.Fields()...).Scan(&endTime)
			if err != nil && !errors.Is(err, pgx.ErrNoRows) {
				log.Println(err)
				if err := tx.Rollback(ctx); err != nil {
					return err
				}
				return err
			}
		}
	}

	return tx.Commit(ctx)
}

func (srv *Chart) Get(ctx context.Context, baseId int, quoteId int, period int64, signature string) ([]*model.OHLC, error) {
	pair := fmt.Sprintf("%d/%d", baseId, quoteId)

	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	statement := fmt.Sprintf(`SELECT %s FROM ohlc c
									WHERE pair_id = $1 AND c.start_time >= $2 AND genesis_signature = $3
 									ORDER BY start_time DESC`,
		model.OHLCColumns())

	rows, err := srv.pool.Query(ctx, statement, pair, period, signature)
	if err != nil && !errors.Is(err, pgx.ErrNoRows) {
		log.Println(err)
		return nil, err
	}

	defer rows.Close()

	var chart []*model.OHLC

	for rows.Next() {
		c := &model.OHLC{}

		err := rows.Scan(c.Fields()...)
		if err != nil {
			log.Println(err)
			return nil, err
		}

		chart = append(chart, c)
	}

	return chart, nil
}
