package sql

import (
	"FoilNetwork/analysis-server/pkg/app/store/model"
	"context"
	"errors"
	"fmt"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"log"
	"time"
)

type Pair struct {
	pool *pgxpool.Pool
}

func NewPairService(pool *pgxpool.Pool) *Pair {
	return &Pair{pool: pool}
}

func (srv *Pair) Insert(ctx context.Context, block *model.Block, pairs []*model.Pair) error {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	tx, err := srv.pool.Begin(ctx)
	if err != nil {
		return err
	}

	for _, pair := range pairs {
		statement := fmt.Sprintf(
			`INSERT INTO pairs (%s) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21) ON CONFLICT DO NOTHING;`,
			model.PairsColumn())

		pair.Name = fmt.Sprintf("%s/%s", pair.BaseName, pair.QuoteName)
		pair.GenesisBlock = block.Signature

		_, err := tx.Exec(ctx, statement, pair.Fields()...)
		if err != nil && !errors.Is(err, pgx.ErrNoRows) {
			log.Println(err)
			if err := tx.Rollback(ctx); err != nil {
				return err
			}
			return err
		}
	}

	if err := tx.Commit(ctx); err != nil {
		return err
	}

	return nil
}

func (srv *Pair) Delete(ctx context.Context) error {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	_, err := srv.pool.Exec(ctx, "DELETE FROM pairs")
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}

func (srv *Pair) Get(ctx context.Context, baseId int, quoteId int, signature string) (*model.Pair, error) {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	statement := `SELECT update_time, base_name, base_id, quote_name, quote_volume, quote_id, last_time, count_24h,
						base_volume, frozen, highest_bid, count_orders_bid, first_price, lowest_price_24h, last_time,
						lowest_ask, highest_price_24h, last_price, count_orders_ask
					FROM pairs
					WHERE base_id = $1 AND quote_id = $2 AND genesis_signature = $3
					ORDER BY update_time DESC LIMIT 1;`

	var p model.Pair

	err := srv.pool.QueryRow(ctx, statement, baseId, quoteId, signature).
		Scan(&p.UpdateTime, &p.BaseName, &p.BaseID, &p.QuoteName, &p.QuoteVolume, &p.QuoteID, &p.LastTime, &p.Count24h,
			&p.BaseVolume, &p.Frozen, &p.HighestBID, &p.CountOrdersBid, &p.FirstPrice, &p.LowestPrice24h, &p.LastTime,
			&p.LowestAsk, &p.HighestPrice24h, &p.LastPrice, &p.CountOrdersAsk)

	if err != nil && !errors.Is(err, pgx.ErrNoRows) {
		log.Println(err)
		return nil, err
	}

	return &p, nil
}

func (srv *Pair) GetAll(ctx context.Context, signature string) ([]*model.Pair, error) {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	statement := `SELECT t.update_time, t.base_name, t.base_id, t.quote_name, t.quote_volume, t.quote_id, t.last_time, t.count_24h,
					   t.base_volume, t.frozen, t.highest_bid, t.count_orders_bid, t.first_price, t.lowest_price_24h, t.last_time,
					   t.lowest_ask, t.highest_price_24h, t.last_price, t.count_orders_ask, t.name

				FROM pairs t, (SELECT update_time FROM pairs WHERE genesis_signature = $1
							 ORDER BY update_time DESC LIMIT 1) dt

				WHERE t.genesis_signature = $1 AND t.update_time = dt.update_time
				ORDER BY t.update_time DESC;`

	rows, err := srv.pool.Query(ctx, statement, signature)
	if err != nil && !errors.Is(err, pgx.ErrNoRows) {
		log.Println(err)
		return nil, err
	}

	defer rows.Close()

	var pairs []*model.Pair

	for rows.Next() {
		p := &model.Pair{}

		err := rows.Scan(&p.UpdateTime, &p.BaseName, &p.BaseID, &p.QuoteName, &p.QuoteVolume, &p.QuoteID, &p.LastTime,
			&p.Count24h, &p.BaseVolume, &p.Frozen, &p.HighestBID, &p.CountOrdersBid, &p.FirstPrice, &p.LowestPrice24h,
			&p.LastTime, &p.LowestAsk, &p.HighestPrice24h, &p.LastPrice, &p.CountOrdersAsk, &p.Name)
		if err != nil {
			log.Println(err)
			return nil, err
		}

		pairs = append(pairs, p)
	}

	return pairs, nil
}

func (srv *Pair) GetAllByBase(ctx context.Context, baseId int, signature string) ([]*model.Pair, error) {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	statement := `SELECT update_time, base_name, base_id, quote_name, quote_volume, quote_id, last_time, count_24h,
						base_volume, frozen, highest_bid, count_orders_bid, first_price, lowest_price_24h, last_time,
						lowest_ask, highest_price_24h, last_price, count_orders_ask
				FROM pairs
				WHERE base_id = $1 AND genesis_signature = $2
				ORDER BY update_time DESC;`

	rows, err := srv.pool.Query(ctx, statement, baseId, signature)
	if err != nil && !errors.Is(err, pgx.ErrNoRows) {
		log.Println(err)
		return nil, err
	}

	defer rows.Close()

	var pairs []*model.Pair

	for rows.Next() {
		p := &model.Pair{}

		err := rows.Scan(&p.UpdateTime, &p.BaseName, &p.BaseID, &p.QuoteName, &p.QuoteVolume, &p.QuoteID, &p.LastTime,
			&p.Count24h, &p.BaseVolume, &p.Frozen, &p.HighestBID, &p.CountOrdersBid, &p.FirstPrice, &p.LowestPrice24h,
			&p.LastTime, &p.LowestAsk, &p.HighestPrice24h, &p.LastPrice, &p.CountOrdersAsk)
		if err != nil {
			log.Println(err)
			return nil, err
		}

		pairs = append(pairs, p)
	}

	return pairs, nil
}

func (srv *Pair) GetAllByQuote(ctx context.Context, quoteId int, signature string) ([]*model.Pair, error) {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	statement := `SELECT update_time, base_name, base_id, quote_name, quote_volume, quote_id, last_time, count_24h,
						base_volume, frozen, highest_bid, count_orders_bid, first_price, lowest_price_24h, last_time,
						lowest_ask, highest_price_24h, last_price, count_orders_ask
				FROM pairs
				WHERE quote_id = $1 AND genesis_signature = $2
				ORDER BY update_time DESC;`

	rows, err := srv.pool.Query(ctx, statement, quoteId, signature)
	if err != nil && !errors.Is(err, pgx.ErrNoRows) {
		log.Println(err)
		return nil, err
	}

	defer rows.Close()

	var pairs []*model.Pair

	for rows.Next() {
		p := &model.Pair{}

		err := rows.Scan(&p.UpdateTime, &p.BaseName, &p.BaseID, &p.QuoteName, &p.QuoteVolume, &p.QuoteID, &p.LastTime,
			&p.Count24h, &p.BaseVolume, &p.Frozen, &p.HighestBID, &p.CountOrdersBid, &p.FirstPrice, &p.LowestPrice24h,
			&p.LastTime, &p.LowestAsk, &p.HighestPrice24h, &p.LastPrice, &p.CountOrdersAsk)
		if err != nil {
			log.Println(err)
			return nil, err
		}

		pairs = append(pairs, p)
	}

	return pairs, nil
}
