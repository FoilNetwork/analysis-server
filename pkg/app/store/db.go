package store

import (
	"FoilNetwork/analysis-server/pkg/app/store/model"
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
)

type DB struct {
	Pool  *pgxpool.Pool
	Trade TradesService
	Pair  PairRepository
	Chart ChartRepository
}

func (db *DB) Close() {
	db.Pool.Close()
}

type TradesService interface {
	GetLast(ctx context.Context, block *model.Block) (*model.Trade, error)
	SaveData(ctx context.Context, block *model.Block, data []*model.Trade) error
	Get(ctx context.Context, baseId int, quoteId int, timestamp int64, signature string) ([]*model.Trade, error)
	GetByTime(ctx context.Context, timestamp int64, signature string) ([]*model.Trade, error)
}

type PairRepository interface {
	Insert(ctx context.Context, block *model.Block, pairs []*model.Pair) error
	Delete(ctx context.Context) error
	Get(ctx context.Context, baseId int, quoteId int, signature string) (*model.Pair, error)
	GetAll(ctx context.Context, signature string) ([]*model.Pair, error)
	GetAllByQuote(ctx context.Context, quoteId int, signature string) ([]*model.Pair, error)
	GetAllByBase(ctx context.Context, baseId int, signature string) ([]*model.Pair, error)
}

type ChartRepository interface {
	GetLast(ctx context.Context, signature string) (map[string]model.OHLC, error)
	Insert(ctx context.Context, block *model.Block, chartsByPair map[string]map[int64]*model.OHLC) error
	Get(ctx context.Context, baseId int, quoteId int, period int64, signature string) ([]*model.OHLC, error)
}
