package model

type Block struct {
	Creator           string  `json:"creator,omitempty"`
	Signature         string  `json:"signature,omitempty"`
	Fee               float64 `json:"fee,omitempty"`
	ForgingValue      int     `json:"forgingValue,omitempty"`
	Version           int     `json:"version,omitempty"`
	WinValue          int     `json:"winValue,omitempty"`
	Target            int     `json:"target,omitempty"`
	Reference         string  `json:"reference,omitempty"`
	EmittedFee        float64 `json:"emittedFee,omitempty"`
	WinValueTargeted  float64 `json:"winValueTargeted,omitempty"`
	TransactionsCount int     `json:"transactionsCount,omitempty"`
	TransactionsHash  string  `json:"transactionsHash,omitempty"`
	Timestamp         int64   `json:"timestamp,omitempty"`
	Height            int64   `json:"height,omitempty"`
}
