package model

import (
	"FoilNetwork/analysis-server/pkg/app/client"
	"encoding/json"
	"log"
	"net/url"
)

type Node struct {
	ConnectionString string `json:"-"`
	NetworkMode      int    `json:"networkMode"`
	ForgingStatus    ForgingStatus
	LastBlock        Block
	RPC              bool `json:"rpc"`
	WEB              bool `json:"web"`
	Version          Version
	Status           string `json:"status"`
	Height           int64  `json:"height"`
}

type ForgingStatus struct {
	Code int    `json:"code"`
	Name string `json:"name"`
}

type Version struct {
	BuildTimeStamp int64  `json:"buildTimeStamp"`
	BuildDate      string `json:"buildDate"`
	Version        string `json:"version"`
}

func NewNode(connectionString string) (*Node, error) {
	resp, err := client.Get(connectionString, "/api/info", nil)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	node := &Node{
		ConnectionString: connectionString,
	}

	if err := json.NewDecoder(resp.Body).Decode(&node); err != nil {
		log.Println(err)
		return nil, err
	}

	return node, nil
}

func (obj *Node) GetGenesisBlock() (*Block, error) {
	var block Block

	resp, err := client.Get(obj.ConnectionString, "/api/firstblock", nil)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if err := json.NewDecoder(resp.Body).Decode(&block); err != nil {
		log.Println(err)
		return nil, err
	}

	obj.LastBlock = block

	return &block, nil
}

func (obj *Node) GetTrades(param url.Values) ([]*Trade, error) {
	var trades []*Trade

	param.Set("limit", "200")
	resp, err := client.Get(obj.ConnectionString, "/apiexchange/tradesfrom", param)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if err := json.NewDecoder(resp.Body).Decode(&trades); err != nil {
		log.Println(err)
		return nil, err
	}

	return trades, nil
}

func (obj *Node) GetPairs() ([]*Pair, error) {
	var pairs *Pairs

	resp, err := client.Get(obj.ConnectionString, "/apiexchange/spot/pairs", nil)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if err := json.NewDecoder(resp.Body).Decode(&pairs); err != nil {
		log.Println(err)
		return nil, err
	}

	return pairs.List(), nil
}
