package model

type Pair struct {
	ID              string  `json:"id,omitempty"`
	Name            string  `json:"name,omitempty"`
	BaseID          int     `json:"base_id,omitempty"`
	BaseName        string  `json:"base_name,omitempty"`
	BaseVolume      float64 `json:"base_volume,omitempty"`
	QuoteID         int     `json:"quote_id,omitempty"`
	QuoteName       string  `json:"quote_name,omitempty"`
	QuoteVolume     float64 `json:"quote_volume,omitempty"`
	LowestAsk       float64 `json:"lowest_ask,omitempty"`
	LastTime        int64   `json:"last_time,omitempty"`
	LowestPrice24h  float64 `json:"lowest_price_24h,omitempty"`
	HighestPrice24h float64 `json:"highest_price_24h,omitempty"`
	FirstPrice      float64 `json:"first_price,omitempty"`
	LastPrice       float64 `json:"last_price,omitempty"`
	CountOrdersAsk  int     `json:"count_orders_ask,omitempty"`
	CountOrdersBid  int     `json:"count_orders_bid,omitempty"`
	HighestBID      float64 `json:"highest_bid,omitempty"`
	Frozen          int     `json:"frozen,omitempty"`
	Count24h        int     `json:"count_24h,omitempty"`
	UpdateTime      int64   `json:"update_time,omitempty"`
	GenesisBlock    string  `json:"genesisBlock,omitempty"`
}

func (p *Pair) Fields() []interface{} {
	return []interface{}{
		&p.ID,
		&p.Name,
		&p.BaseID,
		&p.BaseName,
		&p.BaseVolume,
		&p.QuoteID,
		&p.QuoteName,
		&p.QuoteVolume,
		&p.LowestAsk,
		&p.LastTime,
		&p.LowestPrice24h,
		&p.HighestPrice24h,
		&p.FirstPrice,
		&p.LastPrice,
		&p.CountOrdersAsk,
		&p.CountOrdersBid,
		&p.HighestBID,
		&p.Frozen,
		&p.Count24h,
		&p.UpdateTime,
		&p.GenesisBlock,
	}
}

type Pairs struct {
	ERAUSD   Pair `json:"ERA_USD"`
	COMPURUB Pair `json:"COMPU_RUB"`
	GOLDBTC  Pair `json:"GOLD_BTC"`
	COMPUUSD Pair `json:"COMPU_USD"`
	ERARUB   Pair `json:"ERA_RUB"`
	BTCUSD   Pair `json:"BTC_USD"`
	USDRUB   Pair `json:"USD_RUB"`
	COMPUERA Pair `json:"COMPU_ERA"`
	GOLDRUB  Pair `json:"GOLD_RUB"`
	COMPUBTC Pair `json:"COMPU_BTC"`
	GOLDUSD  Pair `json:"GOLD_USD"`
	ERABTC   Pair `json:"ERA_BTC"`
	BTCRUB   Pair `json:"BTC_RUB"`

	Spot [][]interface{} `json:"spot"`
}

func PairsColumn() string {
	return "pair_id, name, base_id, base_name, base_volume, quote_id, quote_name, quote_volume, lowest_ask, last_time, lowest_price_24h, highest_price_24h, first_price, last_price, count_orders_ask, count_orders_bid, highest_bid, frozen, count_24h, update_time, genesis_signature"
}

func (p *Pairs) List() []*Pair {
	return []*Pair{
		&p.ERAUSD,
		&p.COMPURUB,
		&p.GOLDBTC,
		&p.COMPUUSD,
		&p.ERARUB,
		&p.BTCUSD,
		&p.USDRUB,
		&p.COMPUERA,
		&p.GOLDRUB,
		&p.COMPUBTC,
		&p.GOLDUSD,
		&p.ERABTC,
		&p.BTCRUB,
	}
}
