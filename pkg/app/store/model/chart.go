package model

type OHLC struct {
	PairID       string  `json:"pair_id"`
	StartTime    int64   `json:"start_time"`
	EndTime      int64   `json:"end_time"`
	Count        int     `json:"count"`
	WantVolume   float64 `json:"quote_volume"`
	HaveVolume   float64 `json:"base_volume"`
	Open         float64 `json:"open"`
	TimeOpen     int64   `json:"-"`
	High         float64 `json:"high"`
	Low          float64 `json:"low"`
	Close        float64 `json:"close"`
	TimeClose    int64   `json:"-"`
	GenesisBlock string  `json:"genesisBlock,omitempty"`
	TradesID     string  `json:"-"`
}

func (c *OHLC) Fields() []interface{} {
	return []interface{}{
		&c.PairID, &c.StartTime, &c.EndTime, &c.Count, &c.WantVolume, &c.HaveVolume, &c.Open, &c.High, &c.Low, &c.Close,
		&c.GenesisBlock, &c.TradesID,
	}
}

func (c *OHLC) FillOpenPrice(charts []*OHLC) {
	var t OHLC
	if len(charts) > 0 {
		t = *charts[0]
	}

	for _, k := range charts {
		if k.Open > 0 {
			if t.StartTime > k.StartTime {
				t = *k
			}
		}
	}
	c.Open = t.Open
}

func (c *OHLC) FillHighPrice(charts []*OHLC) {
	var max float64

	for _, t := range charts {
		if max < t.High {
			max = t.High
		}
	}
	c.High = max
}

func (c *OHLC) FillLowPrice(charts []*OHLC) {
	var t OHLC
	if len(charts) > 0 {
		t = *charts[0]
	}

	for _, k := range charts {
		if t.Low > k.Low {
			t = *k
		}
	}
	c.Low = t.Low
}

func (c *OHLC) FillClosePrice(charts []*OHLC) {
	var t OHLC
	if len(charts) > 0 {
		t = *charts[0]
	}

	for _, k := range charts {
		if k.Close > 0 {
			if t.StartTime < k.StartTime {
				t = *k
			}
		}
	}
	c.Close = t.Close
}

// TradeColumns return all columns in string
func OHLCColumns() string {
	return `pair_id, start_time, end_time, count, want_volume, have_volume, open, height, low, close, genesis_signature, trades_id`
}
