package model

import (
	"fmt"
)

type Trade struct {
	ID               string  `json:"id"`
	WantKey          int     `json:"wantKey"`
	Initiator        string  `json:"initiator"`
	InitiatorCreator string  `json:"initiatorCreator"`
	HaveKey          int     `json:"haveKey"`
	AmountHave       float64 `json:"amountHave"`
	Target           string  `json:"target"`
	Sequence         int     `json:"sequence"`
	Price            float64 `json:"price"`
	TargetCreator    string  `json:"targetCreator"`
	AmountWant       float64 `json:"amountWant"`
	Height           int64   `json:"height"`
	Timestamp        int64   `json:"timestamp"`
	ReversePrice     float64 `json:"reversePrice"`
	GenesisBlock     string  `json:"genesisBlock"`
}

//Fields will return all fields of this type
func (t *Trade) Fields() []interface{} {
	return []interface{}{
		&t.ID,
		&t.WantKey,
		&t.Initiator,
		&t.InitiatorCreator,
		&t.HaveKey,
		&t.AmountHave,
		&t.Target,
		&t.Sequence,
		&t.Price,
		&t.TargetCreator,
		&t.AmountWant,
		&t.Height,
		&t.Timestamp,
		&t.ReversePrice,
		&t.GenesisBlock,
	}
}

// TradeFields return all values in string
func (t *Trade) TradeFields() string {
	return fmt.Sprintf("('%v', %v, '%v', '%v', %v, %v, '%v', %v, %v, '%v', %v, %v, %v, %v, %v)",
		t.ID, t.WantKey, t.Initiator, t.InitiatorCreator, t.HaveKey, t.AmountHave, t.Target, t.Sequence, t.Price,
		t.TargetCreator, t.AmountWant, t.Height, t.Timestamp, t.ReversePrice, t.GenesisBlock)
}

// TradeColumns return all columns in string
func TradeColumns() string {
	return `trade_id, want_key, initiator, initiator_creator, have_key, amount_have, target, sequence, price, target_creator, amount_want, height, timestamp, reverse_price, genesis_signature`
}
