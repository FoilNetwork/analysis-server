package store

import (
	"FoilNetwork/analysis-server/pkg/app/store/sql"
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"log"
	"time"
)

func NewDB(dataSourceName string) (*DB, error) {
	conn, err := pgxpool.Connect(context.Background(), dataSourceName)
	if err != nil {
		return nil, err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	_, err = conn.Query(ctx, "SELECT * FROM pg_catalog.pg_tables;")
	if err != nil {
		return nil, err
	}

	log.Println("successfully connected to database")

	return &DB{
		Pool:  conn,
		Trade: sql.NewTradesService(conn),
		Pair:  sql.NewPairService(conn),
		Chart: sql.NewChartRepository(conn),
	}, err
}
