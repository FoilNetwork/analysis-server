package server

import (
	"net/http"
)

func (app *application) Start(addr string) error {
	srv := &http.Server{
		Addr:              addr,
		Handler:           app.routes(),
		ErrorLog:          app.errorLog,
	}

	return srv.ListenAndServe()
}
