package server

import (
	"context"
	"net/http"
	"strings"
)

type key int

const (
	// ContextSignature holds the original request URL
	ContextSignature key = iota
)

func (app *application) genesisSignature(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.Contains(r.URL.Path, "/swagger") {
			next.ServeHTTP(w, r)
			return
		}
		if strings.Contains(r.URL.Path, "/ping") {
			next.ServeHTTP(w, r)
			return
		}

		genSign := r.URL.Query().Get("genesisSignature")
		if genSign == "" {
			app.clientError(w, http.StatusBadRequest)
			return
		}
		next.ServeHTTP(w, r.WithContext(context.WithValue(r.Context(), ContextSignature, genSign)))
	})
}
