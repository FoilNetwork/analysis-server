package server

import (
	_ "FoilNetwork/analysis-server/docs"
	"FoilNetwork/analysis-server/pkg/app/helper"
	"FoilNetwork/analysis-server/pkg/app/store"
	"FoilNetwork/analysis-server/pkg/app/store/model"
	"github.com/gorilla/mux"
	httpSwagger "github.com/swaggo/http-swagger"
	"log"
	"net/http"
	"os"
	"sort"
	"strconv"
)

type application struct {
	debugMode bool
	errorLog  *log.Logger
	infoLog   *log.Logger

	DB *store.DB
}

func New(debugMode bool, store *store.DB) *application {
	return &application{
		debugMode: debugMode,
		infoLog:   log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime),
		errorLog:  log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile),
		DB:        store,
	}
}

func (app *application) routes() http.Handler {
	r := mux.NewRouter()

	r.PathPrefix("/swagger").Handler(httpSwagger.WrapHandler)

	r.HandleFunc("/ping", ping).Methods("GET")
	r.HandleFunc("/trade/base/{base_id}/quote/{quote_id}/{period}", app.getTrade).Methods("GET")

	r.HandleFunc("/pair", app.getAllPairs).Methods("GET")
	r.HandleFunc("/pair/base/{base_id}", app.getAllByBase).Methods("GET")
	r.HandleFunc("/pair/quote/{quote_id}", app.getAllByQuote).Methods("GET")
	r.HandleFunc("/pair/base/{base_id}/quote/{quote_id}", app.getPair).Methods("GET")

	r.HandleFunc("/chart/base/{base_id}/quote/{quote_id}/{period}/{step}", app.getChart).Methods("GET")

	return app.genesisSignature(r)
}

// ping godoc
// @Summary ping-route
// @Description ping the server
// @ID ping-server
// @Produce json
// @Success 200 {string} string "pong"
// @Failure 400 {string} string
// @Router /ping [get]
func ping(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("pong"))
}

//! http://localhost:8080/trade/base/12/quote/95/1D?genesisSignature=V5RkNcBrDi88Tbkm71DN1Po7M7yEop9kpu95gxHjPvR7MDa3uMkYotvGJ8awRTmz2abBEGXShZptrVdzmwuMsag
// getTrade godoc
// @Summary get trades
// @Description get all trades by specified parameters
// @ID get-trades
// @Produce  json
// @Param base_id path integer true "base identifier"
// @Param quote_id path integer true "quote identifier"
// @Param period path string false "period for which need to display trades [allow: default '1D', '1W', '1M', '1Y']"
// @Param genesisSignature query string true "genesis block signature"
// @Success 200 {array} model.Trade
// @Failure 400 {string} string
// @Failure 500 {string} string
// @Router /trade/base/{base_id}/quote/{quote_id}/{period} [get]
func (app *application) getTrade(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	baseID, err := strconv.Atoi(vars["base_id"])
	if err != nil || baseID < 1 {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	quoteID, err := strconv.Atoi(vars["quote_id"])
	if err != nil || quoteID < 1 {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	timestamp := helper.HandlePeriod(vars["period"])

	signature := r.Context().Value(ContextSignature).(string)

	obj, err := app.DB.Trade.Get(r.Context(), baseID, quoteID, timestamp, signature)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.respond(w, obj)
}

//! http://localhost:8080/pair?genesisSignature=V5RkNcBrDi88Tbkm71DN1Po7M7yEop9kpu95gxHjPvR7MDa3uMkYotvGJ8awRTmz2abBEGXShZptrVdzmwuMsag
// getAllPairs godoc
// @Summary get all currency pairs
// @Description get all actual currency pairs
// @ID get-all-pairs
// @Produce  json
// @Param genesisSignature query string true "genesis block signature"
// @Success 200 {array} model.Pair
// @Failure 400 {string} string
// @Failure 500 {string} string
// @Router /pair [get]
func (app *application) getAllPairs(w http.ResponseWriter, r *http.Request) {
	signature := r.Context().Value(ContextSignature).(string)

	obj, err := app.DB.Pair.GetAll(r.Context(), signature)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.respond(w, obj)
}

//! http://localhost:8080/pair/base/12/quote/95?genesisSignature=V5RkNcBrDi88Tbkm71DN1Po7M7yEop9kpu95gxHjPvR7MDa3uMkYotvGJ8awRTmz2abBEGXShZptrVdzmwuMsag
// getAllPairs godoc
// @Summary get currency pair
// @Description get currency pair by base and quote identifiers
// @ID get-pair
// @Produce  json
// @Param base_id path integer true "base identifier"
// @Param quote_id path integer true "quote identifier"
// @Param genesisSignature query string true "genesis block signature"
// @Success 200 {object} model.Pair
// @Failure 400 {string} string
// @Failure 500 {string} string
// @Router /pair/base/{base_id}/quote/{quote_id} [get]
func (app *application) getPair(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	baseID, err := strconv.Atoi(vars["base_id"])
	if err != nil || baseID < 1 {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	quoteID, err := strconv.Atoi(vars["quote_id"])
	if err != nil || quoteID < 1 {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	signature := r.Context().Value(ContextSignature).(string)

	obj, err := app.DB.Pair.Get(r.Context(), baseID, quoteID, signature)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.respond(w, obj)
}

//! http://localhost:8080/pair/base/12?genesisSignature=V5RkNcBrDi88Tbkm71DN1Po7M7yEop9kpu95gxHjPvR7MDa3uMkYotvGJ8awRTmz2abBEGXShZptrVdzmwuMsag
// getAllByBase godoc
// @Summary get currency pair
// @Description get currency pair by base identifier
// @ID get-pair-base
// @Produce  json
// @Param base_id path integer true "base identifier"
// @Param genesisSignature query string true "genesis block signature"
// @Success 200 {array} model.Pair
// @Failure 400 {string} string
// @Failure 500 {string} string
// @Router /pair/base/{base_id} [get]
func (app *application) getAllByBase(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	baseID, err := strconv.Atoi(vars["base_id"])
	if err != nil || baseID < 1 {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	signature := r.Context().Value(ContextSignature).(string)

	obj, err := app.DB.Pair.GetAllByBase(r.Context(), baseID, signature)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.respond(w, obj)
}

//! http://localhost:8080/pair/quote/95?genesisSignature=V5RkNcBrDi88Tbkm71DN1Po7M7yEop9kpu95gxHjPvR7MDa3uMkYotvGJ8awRTmz2abBEGXShZptrVdzmwuMsag
// getAllByQuote godoc
// @Summary get currency pair
// @Description get currency pair by quote identifier
// @ID get-pair-quote
// @Produce  json
// @Param quote_id path integer true "quote identifier"
// @Param genesisSignature query string true "genesis block signature"
// @Success 200 {array} model.Pair
// @Failure 400 {string} string
// @Failure 500 {string} string
// @Router /pair/quote/{quote_id} [get]
func (app *application) getAllByQuote(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	quoteID, err := strconv.Atoi(vars["quote_id"])
	if err != nil || quoteID < 1 {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	signature := r.Context().Value(ContextSignature).(string)

	obj, err := app.DB.Pair.GetAllByQuote(r.Context(), quoteID, signature)
	if err != nil {
		app.serverError(w, err)
		return
	}

	app.respond(w, obj)
}

//! http://localhost:8080/chart/base/12/quote/95/1Y/1h?genesisSignature=V5RkNcBrDi88Tbkm71DN1Po7M7yEop9kpu95gxHjPvR7MDa3uMkYotvGJ8awRTmz2abBEGXShZptrVdzmwuMsag
// getChart godoc
// @Summary get chart
// @Description get data for drawing graphs by specified parameters
// @ID get-chart
// @Produce  json
// @Param base_id path integer true "base identifier"
// @Param quote_id path integer true "quote identifier"
// @Param period path string false "period for which need to display trades [allow: default '1D', '1W', '1M', '1Y']"
// @Param step path string false "trades step [allow: default '1m', '5m', '30m', '1h', '4h', '1D' '1W', '1M']"
// @Param genesisSignature query string true "genesis block signature"
// @Success 200 {array} model.OHLC
// @Failure 400 {string} string
// @Failure 500 {string} string
// @Router /chart/base/{base_id}/quote/{quote_id}/{period}/{step} [get]
func (app *application) getChart(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	baseID, err := strconv.Atoi(vars["base_id"])
	if err != nil || baseID < 1 {
		app.clientError(w, http.StatusBadRequest)
		return
	}

	quoteID, err := strconv.Atoi(vars["quote_id"])
	if err != nil || quoteID < 1 {
		app.clientError(w, http.StatusBadRequest)
		return
	}
	step := helper.HandleStep(vars["step"])

	period := helper.HandlePeriod(vars["period"])

	signature := r.Context().Value(ContextSignature).(string)

	charts, err := app.DB.Chart.Get(r.Context(), baseID, quoteID, period, signature)
	if err != nil {
		app.serverError(w, err)
		return
	}
	//todo: need refactoring
	timeSlots := helper.GenTimeSlots(step, period, helper.MakeTimestamp())

	for keyTime, ohlc := range timeSlots {
		f := []*model.OHLC{}
		for _, chart := range charts {
			if keyTime == helper.GetTimeStart(step, chart.StartTime) {
				ohlc.PairID = chart.PairID
				ohlc.StartTime = helper.GetTimeStart(step, chart.StartTime)
				ohlc.EndTime = helper.GetTimeEnd(step, chart.EndTime)
				ohlc.Count += chart.Count
				ohlc.WantVolume += chart.WantVolume
				ohlc.HaveVolume += chart.HaveVolume

				f = append(f, chart)
			}
		}
		ohlc.FillOpenPrice(f)
		ohlc.FillHighPrice(f)
		ohlc.FillLowPrice(f)
		ohlc.FillClosePrice(f)
	}

	var timeArr []int
	for keyTime := range timeSlots {
		timeArr = append(timeArr, int(keyTime))
	}

	sort.Ints(timeArr)

	for i, timestamp := range timeArr {
		ohlc := timeSlots[int64(timestamp)]

		if ohlc.Count == 0 {

			if i > 0 {
				//todo: if timeArr[-1] (?)
				lastChart := timeSlots[int64(timeArr[i-1])]

				ohlc.PairID = lastChart.PairID
				ohlc.StartTime = int64(timestamp)
				ohlc.EndTime = helper.GetTimeEnd(step, int64(timestamp))
				ohlc.Open = lastChart.Open
				ohlc.High = lastChart.High
				ohlc.Low = lastChart.Low
				ohlc.Close = lastChart.Close
			}
		}
	}

	//выводить в порядке убывания даты
	var arr []*model.OHLC

	sort.Slice(timeArr, func(i, j int) bool {
		return timeArr[i] > timeArr[j]
	})

	for _, timestamp := range timeArr {
		arr = append(arr, timeSlots[int64(timestamp)])
	}

	app.respond(w, arr)
}
