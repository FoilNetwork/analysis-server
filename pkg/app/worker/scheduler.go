package worker

import (
	"github.com/go-co-op/gocron"
	"time"
)

func (w *Worker) Schedule() {
	s := gocron.NewScheduler(time.UTC)

	s.Every(30).Minute().Do(w.SavePairs)
	s.Every(1).Minute().Do(w.SaveChart)
	s.StartAsync()
}
