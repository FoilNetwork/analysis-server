package worker

import (
	"FoilNetwork/analysis-server/pkg/app/store/model"
	"testing"
	"time"
)

const (
	c_pairKey = "12/95"
	c_timeKey = 60000
)

func Test_CreateCharts(t *testing.T) {
	tc := make(map[string][][]*model.Trade)
	mainTrades := []*model.Trade{
		{
			WantKey:    12,
			AmountWant: 0.001,

			HaveKey:    95,
			AmountHave: 7.76597,

			Price:        0.000128767,
			Timestamp:    60001,
			ReversePrice: 7765.97,
		},
		{
			WantKey:    12,
			AmountWant: 0.001,

			HaveKey:    95,
			AmountHave: 7.84402,

			Price:        0.000127486,
			Timestamp:    60009,
			ReversePrice: 7844.02,
		},
	}

	reverseTrades := []*model.Trade{
		{
			WantKey:    95,
			AmountWant: 82.346,

			HaveKey:    12,
			AmountHave: 0.01,

			Price:        8234.6,
			Timestamp:    60006,
			ReversePrice: 0.0001214388,
		},
		{
			WantKey:    95,
			AmountWant: 50.16447,

			HaveKey:    12,
			AmountHave: 0.00759608,

			Price:        6603.99442870533,
			Timestamp:    60001,
			ReversePrice: 0.0001514235,
		},
	}

	tc[c_pairKey] = append(tc[c_pairKey], mainTrades, reverseTrades)

	charts := createCharts(time.Minute, tc)

	for pairKey, chartsByTime := range charts {
		if pairKey != c_pairKey {
			t.Errorf("expected pairKey:%s got:%s", c_pairKey, pairKey)
		}

		for timeKey, charts := range chartsByTime {
			if timeKey != c_timeKey {
				t.Errorf("expected timeKey:%d got:%d", c_timeKey, timeKey)
			}

			if charts.Count != 4 {
				t.Errorf("expected charts.Count:%d got:%d", 4, charts.Count)
			}
			if charts.Open != 7765.97 {
				t.Errorf("expected charts.Open:%v got:%v", 7765.97, charts.Open)
			}
			if charts.High != 8234.6 {
				t.Errorf("expected charts.High:%v got:%v", 8234.6, charts.High)
			}
			if charts.Low != 6603.99442870533 {
				t.Errorf("expected charts.Low:%v got:%v", 6603.99442870533, charts.Low)
			}
			if charts.Close != 7844.02 {
				t.Errorf("expected charts.Low:%v got:%v", 7844.02, charts.Close)
			}
		}
	}
}
