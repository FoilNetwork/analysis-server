package worker

import (
	"FoilNetwork/analysis-server/pkg/app/helper"
	"FoilNetwork/analysis-server/pkg/app/store/model"
	"context"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"
)

func tradesIn(n time.Duration, trades []*model.Trade) map[int64][]*model.Trade {
	tradesMap := make(map[int64][]*model.Trade, 0)

	for _, trade := range trades {
		timeKey := helper.GetTimeStart(n, trade.Timestamp)
		tradesMap[timeKey] = append(tradesMap[timeKey], trade)
	}

	return tradesMap
}

func createCharts(n time.Duration, tradesByPair map[string][][]*model.Trade) map[string]map[int64]*model.OHLC {
	charts := make(map[string]map[int64]*model.OHLC, 0)

	for keyPair, vArr := range tradesByPair {
		mainTradesByTime := tradesIn(n, vArr[0])
		reverseTradesByTime := tradesIn(n, vArr[1])

		chart := make(map[int64]*model.OHLC, 0)

		for keyTime, trades := range mainTradesByTime {
			chart[keyTime] = &model.OHLC{
				PairID:     keyPair,
				StartTime:  keyTime,
				EndTime:    helper.GetTimeEnd(n, keyTime),
				Count:      countTrades(trades),
				WantVolume: getWantVolume(trades),
				HaveVolume: getHaveVolume(trades),
				Open:       getReverseOpenPrice(trades),
				TimeOpen:   getMinTime(trades),
				High:       getReverseMaxPrice(trades),
				Low:        getReverseMinPrice(trades),
				Close:      getReverseClosePrice(trades),
				TimeClose:  getMaxTime(trades),
				TradesID:   getTradesID(trades),
			}
		}

		for keyTime, trades := range reverseTradesByTime {
			val, ok := chart[keyTime]
			if !ok {
				chart[keyTime] = &model.OHLC{
					PairID:    keyPair,
					StartTime: keyTime,
					EndTime:   helper.GetTimeEnd(n, keyTime),
					Count:     countTrades(trades),
					//перевернутые значения
					WantVolume: getHaveVolume(trades),
					HaveVolume: getWantVolume(trades),
					Open:       getOpenPrice(trades),
					High:       getMaxPrice(trades),
					Low:        getMinPrice(trades),
					Close:      getClosePrice(trades),
					TradesID:   getTradesID(trades),
				}
			}

			if ok {
				val.Count += countTrades(trades)
				//перевернутые значения
				val.WantVolume += getHaveVolume(trades)
				val.HaveVolume += getWantVolume(trades)

				reverseMinTime := getMinTime(trades)
				if val.TimeOpen > reverseMinTime {
					val.Open = getOpenPrice(trades)
				}
				reverseMaxPrice := getMaxPrice(trades)
				if val.High < reverseMaxPrice {
					val.High = reverseMaxPrice
				}

				reverseMinPrice := getMinPrice(trades)
				if val.Low > reverseMinPrice {
					val.Low = reverseMinPrice
				}
				reverseMaxTime := getMaxTime(trades)
				if val.TimeClose < reverseMaxTime {
					val.Close = getClosePrice(trades)
				}
				val.TradesID += getTradesID(trades)
			}
		}

		charts[keyPair] = chart
	}

	return charts
}

func (w *Worker) separateTradesByPairs(ctx context.Context, lastCharts map[string]model.OHLC, signature string) (map[string][][]*model.Trade, error) {
	tradesByPair := make(map[string][][]*model.Trade)

	for pairKey, val := range lastCharts {
		ids := strings.Split(pairKey, "/") // '12/95' = 12, 95
		baseID, err := strconv.Atoi(ids[0])
		if err != nil {
			log.Println(err)
			return nil, err
		}

		quoteID, err := strconv.Atoi(ids[1])
		if err != nil {
			log.Println(err)
			return nil, err
		}

		mainTrades, err := w.DB.Trade.Get(ctx, baseID, quoteID, val.EndTime, signature)
		if err != nil {
			log.Println(err)
			return nil, err
		}

		reverseTrades, err := w.DB.Trade.Get(ctx, quoteID, baseID, val.EndTime, signature)
		if err != nil {
			log.Println(err)
			return nil, err
		}

		tradesByPair[pairKey] = append(tradesByPair[pairKey], mainTrades, reverseTrades)
	}
	return tradesByPair, nil
}

func getTradesID(trades []*model.Trade) string {
	ids := ""
	for _, trade := range trades {
		if trade.ID != "" {
			ids += fmt.Sprintf("%s ", trade.ID)
		}
	}
	return ids
}

func countTrades(trades []*model.Trade) int {
	return len(trades)
}

func getWantVolume(trades []*model.Trade) float64 {
	var sum float64
	for _, trade := range trades {
		sum += trade.AmountWant
	}
	return sum
}

func getHaveVolume(trades []*model.Trade) float64 {
	var sum float64
	for _, trade := range trades {
		sum += trade.AmountHave
	}
	return sum
}

func getMaxPrice(trades []*model.Trade) float64 {
	var max float64

	for _, trade := range trades {
		if max < trade.Price {
			max = trade.Price
		}
	}
	return max
}

func getReverseMaxPrice(trades []*model.Trade) float64 {
	var max float64

	for _, trade := range trades {
		if max < trade.ReversePrice {
			max = trade.ReversePrice
		}
	}
	return max
}

func getMinPrice(trades []*model.Trade) float64 {
	var min float64
	if len(trades) > 0 {
		min = trades[0].Price
	}

	for _, trade := range trades {
		if trade.Price > 0 {
			if min > trade.Price {
				min = trade.Price
			}
		}
	}
	return min
}

func getReverseMinPrice(trades []*model.Trade) float64 {
	var min float64
	if len(trades) > 0 {
		min = trades[0].ReversePrice
	}

	for _, trade := range trades {
		if trade.ReversePrice > 0 {
			if min > trade.ReversePrice {
				min = trade.ReversePrice
			}
		}
	}
	return min
}

func getClosePrice(trades []*model.Trade) float64 {
	var t model.Trade

	for _, trade := range trades {
		if trade.Price > 0 {
			if t.Timestamp < trade.Timestamp {
				t = *trade
			}
		}
	}
	return t.Price
}

func getReverseClosePrice(trades []*model.Trade) float64 {
	var t model.Trade

	for _, trade := range trades {
		if trade.ReversePrice > 0 {
			if t.Timestamp < trade.Timestamp {
				t = *trade
			}
		}
	}
	return t.ReversePrice
}

func getOpenPrice(trades []*model.Trade) float64 {
	var t model.Trade
	if len(trades) > 0 {
		t = *trades[0]
	}

	for _, trade := range trades {
		if trade.Price > 0 {
			if t.Timestamp > trade.Timestamp {
				t = *trade
			}
		}
	}
	return t.Price
}

func getReverseOpenPrice(trades []*model.Trade) float64 {
	var t model.Trade
	if len(trades) > 0 {
		t = *trades[0]
	}

	for _, trade := range trades {
		if trade.ReversePrice > 0 {
			if t.Timestamp > trade.Timestamp {
				t = *trade
			}
		}
	}
	return t.ReversePrice
}

func getMaxTime(trades []*model.Trade) int64 {
	var max int64

	for _, trade := range trades {
		if max < trade.Timestamp {
			max = trade.Timestamp
		}
	}

	return max
}

func getMinTime(trades []*model.Trade) int64 {
	var min int64
	if len(trades) > 0 {
		min = trades[0].Timestamp
	}

	for _, trade := range trades {
		if trade.Timestamp > 0 {
			if min > trade.Timestamp {
				min = trade.Timestamp
			}
		}
	}
	return min
}
