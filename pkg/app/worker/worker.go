package worker

import (
	"FoilNetwork/analysis-server/pkg/app/config"
	"FoilNetwork/analysis-server/pkg/app/store"
	"FoilNetwork/analysis-server/pkg/app/store/model"
	"context"
	"fmt"
	"log"
	"net/url"
	"time"
)

type Worker struct {
	DB               *store.DB
	job              func(url string) error
	interval         time.Duration
	urls             []string
	connectionString string
}

func New(db *store.DB, interval time.Duration, urls []config.Node) *Worker {
	var u []string
	for _, url := range urls {
		if url.URL != "" {
			u = append(u, url.URL)
		}
	}

	return &Worker{
		DB:       db,
		interval: interval,
		urls:     u,
	}
}

func (w *Worker) Setup() {
	w.job = func(u string) error {
		w.connectionString = u

		node, err := model.NewNode(w.connectionString)
		if err != nil {
			return err
		}

		genBlock, err := node.GetGenesisBlock()
		if err != nil {
			return err
		}

		param := url.Values{}
		//u := fmt.Sprintf("%s/apiexchange/tradesfrom?trade=%s", w.urls[i], param)

		for {
			trade, err := w.DB.Trade.GetLast(context.TODO(), genBlock)
			if err != nil {
				return err
			}

			lastID := trade.ID

			param.Set("trade", lastID)
			//log.Printf("next param: %+v\n", param)

			trades, err := node.GetTrades(param)
			if err != nil {
				return err
			}

			if len(trades) > 0 {
				nextID := trades[len(trades)-1].ID
				log.Println(lastID, nextID)

				if lastID != nextID {
					err := w.DB.Trade.SaveData(context.TODO(), genBlock, trades)
					if err != nil {
						return err
					}

				}
			}

			time.Sleep(w.interval * time.Second)
		}
	}

	go w.start()
	w.Schedule()
}

func (w *Worker) start() {
	for i := 0; i < len(w.urls); {
		err := w.job(w.urls[i])
		if err != nil {
			log.Println(err)
			i++
			if i == len(w.urls) {
				i = 0
			}
			continue
		}
	}
}

func (w *Worker) SaveChart() {
	node, err := model.NewNode(w.connectionString)
	if err != nil {
		log.Println(err)
		return
	}

	genBlock, err := node.GetGenesisBlock()
	if err != nil {
		log.Println(err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	pairs, err := w.DB.Pair.GetAll(ctx, genBlock.Signature)
	if err != nil {
		log.Println(err)
		return
	}

	lastCharts, err := w.DB.Chart.GetLast(ctx, genBlock.Signature)
	if err != nil {
		log.Println(err)
		return
	}

	//вставить нули там, для тех пар, где нет сделок
	for _, pair := range pairs {
		pairKey := fmt.Sprintf("%d/%d", pair.BaseID, pair.QuoteID)
		_, ok := lastCharts[pairKey]
		if !ok {
			lastCharts[pairKey] = model.OHLC{
				PairID:  pairKey,
				EndTime: 0,
			}
		}
	}

	tradesByPair, err := w.separateTradesByPairs(ctx, lastCharts, genBlock.Signature)
	if err != nil {
		log.Println(err)
		return
	}

	charts := createCharts(time.Minute, tradesByPair)

	//
	err = w.DB.Chart.Insert(ctx, genBlock, charts)
	if err != nil {
		log.Println(err)
		return
	}
}

func (w *Worker) SavePairs() {
	node, err := model.NewNode(w.connectionString)
	if err != nil {
		log.Println(err)
		return
	}

	genBlock, err := node.GetGenesisBlock()
	if err != nil {
		log.Println(err)
		return
	}

	//save pairs
	pairs, err := node.GetPairs()
	if err != nil {
		log.Println(err)
		return
	}

	err = w.DB.Pair.Insert(context.TODO(), genBlock, pairs)
	if err != nil {
		log.Println(err)
		return
	}
}
