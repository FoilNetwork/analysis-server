package worker

import (
	"FoilNetwork/analysis-server/pkg/app/config"
	"FoilNetwork/analysis-server/pkg/app/store"
	"testing"
	"time"
)

func TestQueue_SetupCron(t *testing.T) {
	nodes := []config.Node{
		{"http://localhost:8000/url/to/do/something/1"},
		{"http://localhost:8000/url/to/do/something/2"},
		{"http://localhost:8000/url/to/do/something/3"},
		{"http://127.0.0.1:9047/apiexchange/tradesfrom?trade=1217-1/960-1"},
		{"http://localhost:8000/url/to/do/something/5"},
	}

	db := &store.DB{}


	w := New(db,5, nodes)
	w.Setup()

	ch := make(chan bool)
	time.AfterFunc(17*time.Second, func() {
		ch <- true
	})

	<-ch
}
