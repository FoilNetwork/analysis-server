package helper

import (
	"FoilNetwork/analysis-server/pkg/app/store/model"
	"time"
)

func GenTimeSlots(n time.Duration, from, to int64) map[int64]*model.OHLC {
	//fmt.Printf("n:%v from:%d to:%d\n", n.Milliseconds(), from, to)
	var timeSlots []int64

	for from < GetTimeEnd(n, to) {
		timeSlots = append(timeSlots, GetTimeStart(n, from))
		from += n.Milliseconds()
	}

	charts := make(map[int64]*model.OHLC)

	//TODO: убрать значения из из интревала, периода, который не закрыт
	for _, timestamp := range timeSlots {
		charts[timestamp] = &model.OHLC{}
	}

	return charts
}

func GetTimeStart(n time.Duration, milliseconds int64) int64 {
	// remove everything less than one day
	return milliseconds / n.Milliseconds() * n.Milliseconds()
}

func GetTimeEnd(n time.Duration, milliseconds int64) int64 {
	// remove everything less than one day and add one day milliseconds
	return GetTimeStart(n, milliseconds) + n.Milliseconds()
}

func MakeTimestamp() int64 {
	return time.Now().UTC().UnixNano() / int64(time.Millisecond)
}

func HandlePeriod(period string) int64 {
	now := time.Now().UTC()
	currentYear, currentMonth, currentDay := now.Date()

	switch period {
	case "1Y":
		return time.Date(currentYear-1, currentMonth, currentDay, now.Hour(), now.Minute(), now.Second(), now.Nanosecond(), time.UTC).UnixNano() / int64(time.Millisecond)
	case "1M":
		return time.Date(currentYear, currentMonth-1, currentDay, now.Hour(), now.Minute(), now.Second(), now.Nanosecond(), time.UTC).UnixNano() / int64(time.Millisecond)
	case "1W":
		return time.Date(currentYear, currentMonth, currentDay-7, now.Hour(), now.Minute(), now.Second(), now.Nanosecond(), time.UTC).UnixNano() / int64(time.Millisecond)
	default:
		//1D
		return time.Date(currentYear, currentMonth, currentDay-1, now.Hour(), now.Minute(), now.Second(), now.Nanosecond(), time.UTC).UnixNano() / int64(time.Millisecond)
	}
}

func HandleStep(step string) time.Duration {
	switch step {
	case "5m":
		return 5 * time.Minute
	case "30m":
		return 30 * time.Minute
	case "1h":
		return time.Hour
	case "4h":
		return 4 * time.Hour
	case "1D":
		return 24 * time.Hour
	case "1W":
		return 168 * time.Hour
	case "1M":
		return 720 * time.Hour

	default:
		//1m
		return time.Minute
	}
}
