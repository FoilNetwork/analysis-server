package client

import (
	"crypto/tls"
	"fmt"
	"log"
	"net"
	"net/http"
	"net/url"
	"time"
)

func Get(connectionString string, path string, param url.Values) (*http.Response, error) {
	conf := &tls.Config{
		InsecureSkipVerify: true,
	}

	var netTransport = &http.Transport{
		DialContext: (&net.Dialer{
			Timeout: 5 * time.Second,
		}).DialContext,
		TLSHandshakeTimeout: 10 * time.Second,
		TLSClientConfig:     conf,
	}

	var netClient = &http.Client{
		Timeout:   time.Second * 15,
		Transport: netTransport,
	}

	var (
		err      error
		response *http.Response
	)

	url := fmt.Sprintf("%s%s?%s", connectionString, path, param.Encode())
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return response, err
	}

	req.Header.Add("Content-Type", "application/json")

	response, err = netClient.Do(req)
	if err != nil {
		log.Println(err)
		return response, err
	}

	if response.StatusCode >= 400 {
		err := fmt.Errorf("HTTP request to %s failed with HTTP status %d", connectionString, response.StatusCode)
		log.Println(err)
		return response, err
	}

	// log.Printf("GET %s:\n", url)
	return response, nil
}


func GetHTTPS(connectionString string, path string) (*http.Response, error) {

	conf := &tls.Config{
		InsecureSkipVerify: true,
	}

	var netTransport = &http.Transport{
		Dial: (&net.Dialer{
			Timeout: 5 * time.Second,
		}).Dial,
		TLSHandshakeTimeout: 10 * time.Second,
		TLSClientConfig:     conf,
	}

	var netClient = &http.Client{
		Timeout:   time.Second * 15,
		Transport: netTransport,
	}

	var (
		err      error
		response *http.Response
	)

	url := fmt.Sprintf("%s%s", connectionString, path)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return response, err
	}

	req.Header.Add("Content-Type", "application/json")

	response, err = netClient.Do(req)
	if err != nil {
		return response, err
	}

	defer response.Body.Close()

	if response.StatusCode >= 400 {
		err := fmt.Errorf("HTTP request to %s failed with HTTP status %d", connectionString, response.StatusCode)
		return response, err
	}

	// log.Printf("GET %s:\n", url)
	return response, nil
}
