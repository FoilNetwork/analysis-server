package config

import (
	"github.com/spf13/viper"
	"time"
)

type Config struct {
	Addr      string        `mapstructure:"addr"`
	DebugMode bool          `mapstructure:"debug_mode"`
	Interval  time.Duration `mapstructure:"req_interval"`
	Nodes     []Node        `mapstructure:"nodes"`
}

type Node struct {
	URL string `mapstructure:"url"`
}

func Init() (*Config, error) {
	viper.AddConfigPath("configs")
	viper.SetConfigName("config")

	if err := viper.ReadInConfig(); err != nil {
		return nil, err
	}

	var cfg Config

	if err := viper.Unmarshal(&cfg); err != nil {
		return nil, err
	}

	return &cfg, nil
}
